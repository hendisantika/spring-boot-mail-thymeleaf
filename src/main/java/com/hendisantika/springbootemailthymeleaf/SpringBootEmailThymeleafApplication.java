package com.hendisantika.springbootemailthymeleaf;

import com.hendisantika.springbootemailthymeleaf.mail.EmailService;
import com.hendisantika.springbootemailthymeleaf.mail.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class SpringBootEmailThymeleafApplication implements ApplicationRunner {

    private static Logger log = LoggerFactory.getLogger(SpringBootEmailThymeleafApplication.class);

    @Autowired
    private EmailService emailService;


    public static void main(String[] args) {
        SpringApplication.run(SpringBootEmailThymeleafApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Sending Email with Thymeleaf HTML Template Example");

        Mail mail = new Mail();
        mail.setFrom("hendisantika@gmail.com");
        mail.setTo("hendisantika@yahoo.co.id");
        mail.setSubject("Sending Email with Thymeleaf HTML Template Example");

        Map model = new HashMap();
        model.put("name", "hendisantika.wordpress.com");
        model.put("location", "Cimahi, West Java - Indonesia");
        model.put("signature", "https://hendisantika.wordpress.com");
        mail.setModel(model);

        log.info("name : {}", mail.getModel());

        log.info("Data --> " + mail);

        emailService.sendSimpleMessage(mail);
    }
}
